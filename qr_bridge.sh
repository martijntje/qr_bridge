#!/bin/bash

echo "Scanning QR from smartwatch"

LD_PRELOAD=/usr/lib/libv4l/v4l1compat.so zbarcam /dev/video0 --raw |\
    head -n 1 | head -c 20 | tr ' ' '\n' > /tmp/args.txt
echo >> /tmp/args.txt           # ensure a final newline
IP=`cat /tmp/args.txt | head -n 1 `
PORT=`cat /tmp/args.txt | head -n 2 | tail -n 1`
echo "Smartwatch found at " $IP":"$PORT
sleep 1
echo "Taking screenshot of screen"
scrot /tmp/screenshot.png

# find contents of qr code from screenshot
zbarimg -q --raw /tmp/screenshot.png |\
    sed 's/,/,\n/g' |\
    sed 's/\"\(.\+\)":"\(.\+\)"/ \1: '\''\2'\''/g' > /tmp/qr.txt
echo >> /tmp/qr.txt
echo >> /tmp/qr.txt
echo "sending message to smartwatch"
cat /tmp/qr.txt | nc $IP $PORT
